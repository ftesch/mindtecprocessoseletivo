/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

//import das Bibliotecas

import VueRouter from 'vue-router'
Vue.use(VueRouter)

// https://www.npmjs.com/package/vue-filter-date-format  :: pacote para para use com datas
import VueMoment from 'vue-moment'
Vue.use(VueMoment)

//https://bootstrap-vue.js.org/
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)

// https://vuelidate.netlify.com
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)


// https://www.npmjs.com/package/vue-loading-overlay
//import VueLoading from 'vue-loading-overlay';
import Loading from 'vue-loading-overlay'
Vue.use(Loading)

 //import dos Compoentes

 import App from './views/App'
 
 import ListClientes from './components/Clientes/ListClientes'
 import CreateClientes from './components/Clientes/CreateClientes'
 import EditClientes from './components/Clientes/EditClientes'
 
 import ListContatos from './components/ContatosClientes/ListContatos'
 import CreateContatos from './components/ContatosClientes/CreateContatos'
 import EditContatos from './components/ContatosClientes/EditContatos'
 

 const router = new VueRouter({
     mode: 'history',
     routes : [
       /* Rotas para Cadastro de Clientes */
        { path : '/listclientes', component : ListClientes },
        { path : '/clientes/create', component : CreateClientes },
        { name: 'EditCliente', path : '/clientes/:id/edit', component : EditClientes },
       /* Rotas para Contatos */
       { path : '/listcontatos', component : ListContatos },
        { path : '/contatos/create', component : CreateContatos },
        { name: 'EditContato', path : '/contatos/:id/edit', component : EditContatos },
    ]
  })

const app = new Vue({
    el: '#app',
    components: { App },
    router
  });

export default app;  