<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ContatosClientes extends Model
{
    use SoftDeletes;

    protected $table = 'ContatosClientes';
    protected $primaryKey = 'idContato';
    public $timestamps = false;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'idContato',
        'idCliente',
        'TipoContato',
        'DescContato',
        'BolAtivo',
    ];

    protected $appends = ['Descricao_Situacao'];

    public function getDescricaoSituacaoAttribute()
    {
        return ($this->BolAtivo == 1) ? 'Ativo' : 'Inativo';
    }


    public function Cliente()
    {
        return $this->belongsTo(\App\Clientes::class, 'idCliente', 'idCliente');
    }

}
