<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clientes;
use App\ContatosClientes;

class APIContatosClientesController extends Controller
{
    // retornar todos os clientes
    public function getAll()
    {
        $contatos = ContatosClientes::all();
        $retorno = [];
        foreach ($contatos as $contato) {
            $retorno[] = [
                'idContato' => $contato->idContato,
                'TipoContato' => $contato->TipoContato,
                'DescContato' => $contato->DescContato,
                'Cliente' => $contato->Cliente->RazaoSocial,
                'Descricao_Situacao' => $contato->Descricao_Situacao,
            ];
        }

        return response()->json($retorno);
    }

    public function store(Request $request)
    {
        $validate = $request->validate([
            'idCliente' => 'required',
            'TipoContato' => 'required',
            'DescContato' => 'required',
            'BolAtivo' => 'required',
        ]);

        $contato = new ContatosClientes();
        $contato->TipoContato = $request["TipoContato"];
        $contato->DescContato = $request["DescContato"];
        $contato->BolAtivo = $request["BolAtivo"];
        $contato->idCliente = $request["idCliente"];
        $contato->save();

        return response()->json(['idContato' => $contato->idContato]);
    }

    public function getContato($idContato)
    {
        $contato = ContatosClientes::find($idContato);
        $clientes = Clientes::all();
        return response()->json(["contato" => $contato, "clientes" => $clientes]);
    }

    public function update(Request $request)
    {
        $validate = $request->validate([
            'idCliente' => 'required',
            'TipoContato' => 'required',
            'DescContato' => 'required',
            'BolAtivo' => 'required',
        ]);

        $contato = ContatosClientes::find($request["idContato"]);
        $contato->TipoContato = $request["TipoContato"];
        $contato->DescContato = $request["DescContato"];
        $contato->BolAtivo = $request["BolAtivo"];
        $contato->idCliente = $request["idCliente"];
        $contato->save();

        return response()->json(['idContato' => $contato->idContato]);

    }

    public function delete($idContato)
    {
        ContatosClientes::destroy($idContato);
        return response()->json(['retorno' => 1]);
    }

}
