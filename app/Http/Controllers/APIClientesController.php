<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clientes;

class APIClientesController extends Controller
{
    // retornar todos os clientes
    public function getAll()
    {
        $clientes = Clientes::All();
        return response()->json($clientes);
    }

    public function store(Request $request)
    {
        $validate = $request->validate([
            'RazaoSocial' => 'required',
            'BolAtivo' => 'required',
        ]);

        $cliente = new Clientes();
        $cliente->RazaoSocial = $request["RazaoSocial"];
        $cliente->BolAtivo = $request["BolAtivo"];
        $cliente->DataCadastro = \Carbon\Carbon::now();
        $cliente->save();

        return response()->json(['idCliente' => $cliente->idCliente]);
    }

    public function getCliente($idCliente)
    {
        $cliente = Clientes::find($idCliente);
        return response()->json($cliente);
    }

    public function update(Request $request)
    {
        $validate = $request->validate([
            'RazaoSocial' => 'required',
            'BolAtivo' => 'required',
        ]);

        $cliente = Clientes::find($request["idCliente"]);
        $cliente->RazaoSocial = $request["RazaoSocial"];
        $cliente->BolAtivo = $request["BolAtivo"];
        $cliente->save();

        return response()->json(['idCliente' => $cliente->idCliente]);

    }

    public function delete($idCliente)
    {
        Clientes::destroy($idCliente);
        return response()->json(['retorno' => 1]);
    }


}
