<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clientes extends Model
{
    use SoftDeletes;

    protected $table = 'clientes';
    protected $primaryKey = 'idCliente';
    public $timestamps = false;

    protected $dates = ['deleted_at', 'DataCadastro'];

    protected $fillable = [
        'idCliente',
        'RazaoSocial',
        'DataCadastro',
        'BolAtivo',
    ];

    protected $appends = ['Descricao_Situacao'];

    public function getDescricaoSituacaoAttribute()
    {
        return ($this->BolAtivo == 1) ? 'Ativo' : 'Inativo';
    }


    public function Contatos()
    {
        return $this->hasMany(\App\ContatosClientes::class, 'idCliente', 'idCliente');
    }



}
