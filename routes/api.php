<?php

use Illuminate\Http\Request;


/*
APIs para resposta do Cadastro de Clientes
*/

Route::get('/clientes/getAll', 'APIClientesController@getAll');
Route::get('/clientes/{idCliente}/getCliente', 'APIClientesController@getCliente');
Route::put('/clientes/update', 'APIClientesController@update');
Route::post('/clientes/store', 'APIClientesController@store');
Route::delete('/clientes/{idCliente}/delete', 'APIClientesController@delete');


/*
APIs para resposta do Cadastro Contatos
*/

Route::get('/contatos/getAll', 'APIContatosClientesController@getAll');
Route::get('/contatos/{idContato}/getContato', 'APIContatosClientesController@getContato');
Route::put('/contatos/update', 'APIContatosClientesController@update');
Route::post('/contatos/store', 'APIContatosClientesController@store');
Route::delete('/contatos/{idContato}/delete', 'APIContatosClientesController@delete');
