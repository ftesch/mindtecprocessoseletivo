## Processo Seletivo Mintec

## Repositorio do Projeto
https://bitbucket.org/ftesch/mindtecprocessoseletivo.git
 
## Quer ver a aplicação rodando?, acesse
*Publicado em : http://mindtec.meugestorweb.net.br*
 
## SQL para criação do Banco de dados
 
-- MySQL Workbench Synchronization  
-- Generated: 2019-10-27 13:39  
-- Model: New Model  
-- Version: 1.0  
-- Project: Mintec Processo Seletivo  
-- Author: ftesch  

CREATE SCHEMA IF NOT EXISTS `mindtecprocsel` DEFAULT CHARACTER SET utf8 ;  

CREATE TABLE IF NOT EXISTS `mindtecprocsel`.`Clientes` (   
  `idCliente` INT(11) NOT NULL AUTO_INCREMENT,  
  `RazaoSocial` VARCHAR(45) NULL DEFAULT NULL,  
  `DataCadastro` DATE NULL DEFAULT NULL,  
  `BolAtivo` INT(11) NULL DEFAULT NULL,  
  `deleted_at` TIMESTAMP NULL DEFAULT NULL,  
  PRIMARY KEY (`idCliente`))  
ENGINE = InnoDB  
DEFAULT CHARACTER SET = utf8;  

CREATE TABLE IF NOT EXISTS `mindtecprocsel`.`ContatosClientes` (  
  `idContato` INT(11) NOT NULL AUTO_INCREMENT,  
  `idCliente` INT(11) NOT NULL,  
  `TipoContato` VARCHAR(45) NULL DEFAULT NULL,  
  `DescContato` VARCHAR(45) NULL DEFAULT NULL,  
  `BolAtivo` INT(11) NULL DEFAULT NULL,  
  `deleted_at` TIMESTAMP NULL DEFAULT NULL,  
  PRIMARY KEY (`idContato`),  
  INDEX `fk_ContatosClientes_Clientes_idx` (`idCliente` ASC) ,  
  CONSTRAINT `fk_ContatosClientes_Clientes`  
    FOREIGN KEY (`idCliente`)  
    REFERENCES `mindtecprocsel`.`Clientes` (`idCliente`)  
    ON DELETE NO ACTION  
    ON UPDATE NO ACTION)  
ENGINE = InnoDB  
DEFAULT CHARACTER SET = utf8;  

## Configurações

### Configure .env

Copie e cole o .env abaixo como exemplo: (.env padrão de instalação laravel)
  
APP_NAME=Laravel  
APP_ENV=local  
APP_KEY=  
APP_DEBUG=true  
APP_URL=http://localhost  
  
LOG_CHANNEL=stack  
  
DB_CONNECTION=mysql  
DB_HOST=127.0.0.1  
DB_PORT=3306  
DB_DATABASE=laravel  
DB_USERNAME=root  
DB_PASSWORD=  
  
BROADCAST_DRIVER=log  
CACHE_DRIVER=file  
QUEUE_CONNECTION=sync  
SESSION_DRIVER=file  
SESSION_LIFETIME=120  
  
REDIS_HOST=127.0.0.1  
REDIS_PASSWORD=null  
REDIS_PORT=6379  
  
MAIL_DRIVER=smtp  
MAIL_HOST=smtp.mailtrap.io  
MAIL_PORT=2525  
MAIL_USERNAME=null  
MAIL_PASSWORD=null  
MAIL_ENCRYPTION=null  
  
AWS_ACCESS_KEY_ID=  
AWS_SECRET_ACCESS_KEY=  
AWS_DEFAULT_REGION=us-east-1  
AWS_BUCKET=  
  
PUSHER_APP_ID=  
PUSHER_APP_KEY=  
PUSHER_APP_SECRET=  
PUSHER_APP_CLUSTER=mt1  
  
MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"  
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"  

### Ajuste as credenciais de conexão com o banco de dados MySQL
Atenção, seguindo o script de criação do banco de dados acima o banco de dados deve-se chamar *mindtecprocsel*


## Instalar Bibliotecas Laravel
composer install

## Instalar Bibliotecas Javascript
npm install

## Gerar key da Aplicação Laravel
php artisan key:generate

## Compilar app.js da aplicação

*Em Produção:* npm run production  
*Em Desenvolvimento:* npm run development


## Atenção é necessário dar permissão na masta storage do laravel
sudo chmod 777 -R storage/

## Observação
 Para atender a especificação de exclusão *lógica* de registro, foi necessário criar mais um campo em cada tabela, por isso o script também cria o campodeleted_at (nomenclarura padrão Laravel)

## Agradecimentos
Quero agradecer desde já a oportunidade de participar desse processo seletivo.
 